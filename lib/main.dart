import 'package:flutter/material.dart';

class TutorialHome extends StatelessWidget{
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        leading: IconButton(
          icon: Icon(Icons.menu),
          tooltip: 'Navigation Menu',
          onPressed: null
          ),
        title: Text('Example title'),
        actions: [
          IconButton(onPressed: null, icon: Icon(Icons.search), tooltip: 'search',)
        ],
        ),
      body: Center(child: Text('Hello, World!')),
      floatingActionButton: FloatingActionButton(
        child: Icon(Icons.add),
        tooltip: 'Add',
        onPressed: null,
      )
    );
  }
}

void main(){
  runApp(
    MaterialApp(
      title: 'Flutter Tutorial',
      home: TutorialHome()
    ),
  );
}

